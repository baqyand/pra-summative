
public class Day5Task152 {
    String merek;
    int harga,total;
    public Day5Task152(String merek, int harga){
        this.merek = merek;
        this.harga = harga;
        total = hitungTotal();
    }
    private int hitungTotal(){
        return harga* 10;
    }
}

class Mobil extends Day5Task152{
    public Mobil(String merek, int harga){
        super(merek, harga);
        System.out.println(merek + " : " + total);
    }
}