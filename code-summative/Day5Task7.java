
public class Day5Task7 {
    public static void main(String[] args){
        //inisialisasi array dan mengisi data array nya
        int[] array = {3, 4, 1, 5, 2, 19, 9};
        int[] array2 = {1, 2, 3, 4, 5, 6, 7};

        System.out.println("\nArray tambah ");

        //output dari fungsi yang di isi data array
        tambahArr(array,array2);
    }
    // membuat function tambahArr yang sesuai dengan length nya
    public static void tambahArr(int[] arr1, int[] arr2) {
        int length;

        // membuat expression untuk menyamakan length
        if (arr1.length < arr2.length) {
            length = arr1.length;
        } else {
            length = arr2.length;
        }
        int[] hasil = new int[length];

        //looping untuk pertambahan
        // di mana hasil = arr 1 + arr 2
        for (int i = 0; i < length; i++) {
            hasil[i] = arr1[i] + arr2[i];
        }
        printArr(hasil);
    }

    //function untuk looping array nnya
    public static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
