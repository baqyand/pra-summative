
public class Day5Task17 implements Tas, Dompet {

    public static void main(String[] args) {
        Day5Task17 day = new Day5Task17();
        System.out.println("object 1 = Tas");
        day.dipakai();
        day.jenis();
        System.out.println("\n\n ");
        System.out.println("object 2 = Dompet");
        day.harga();
        day.isi();
    }
    public void dipakai() {
        System.out.println("Tas ini di pakai di saat penting");
    }
    public void jenis() {
        System.out.println("Tas terdapat berbagai macam jenis ");
    }
    public void harga() {
        System.out.println("Dompet ini memiliki harga bervariatif");
    }
    public void isi() {
        System.out.println("isi : kartu, uang dan lainnya");
    }
}

interface Tas {
    void dipakai();
    void isi();
}

interface Dompet {
    void harga();
    void jenis();
}
