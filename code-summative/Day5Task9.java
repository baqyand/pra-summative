import java.util.HashMap;
import java.util.Map;

public class Day5Task9 {
    public static void main(String[] args) {
        HashMap<String, Buku> bk = new HashMap<String, Buku>();
        // mengisi nilai ke objek days
	
	//menyiapkan data untuk hashmap
        Buku laskar = new Buku(" Laskar pelangi ","Bama");
        Buku program = new Buku(" Java Programming ","Qyan");
        Buku kocheng = new Buku(" Kocheng ","Deandra");

        // mengisi objek hashmap dengan objek buku
        bk.put("novel", laskar);
        bk.put("Program", program);
        bk.put("Kehidupan", kocheng);

        System.out.println("Size buku : "+ bk.size());
	
	//looping untuk mengetahui key ("string", tittle)
        for(Map.Entry b: bk.entrySet()){
            Buku buku = (Buku) b.getValue();
            System.out.println(b.getKey() + ": "+ buku.getTitle());
        }
    }
}

class Buku {

    private String title;
    private String author;

    public Buku(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}

