
public class Day5Task1 {
    // instance variable
    int myVar1 = 10;
    int myvar2 = 20;

    //static variable
    static int data = 30;

    public static void main(String[] args) {
        //inisialisasi object
        Day5Task1 day5Task1 = new Day5Task1();
        //Output dari variable
        hitung(day5Task1.myVar1, day5Task1.myvar2);
        //output dari static variable
        System.out.println("nilai static : " + data);
        //output dari intance variable
        System.out.println("nilai intance : " + day5Task1.myVar1 + " , " + day5Task1.myvar2);

    }

    public static void hitung(int a, int b) {
        //local variable
        int hasil;

        hasil = a + b;
	//print out hasil dari pertambahan

        System.out.println("hasil pertambahan dari data : " + hasil);

    }
}
