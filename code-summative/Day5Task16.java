
public class Day5Task16 {
    public static void main(String [] args){
        // object kelas parent ke child
        Ikan ikan = new Cupang();
        ikan.name = "Cupang";

        //object child - child
        Cupang cupang = new Cupang();
        cupang.name = "Halfmoon";
        cupang.warna  = "Pink";

        // output dari properti ikan
        System.out.println("Object ikan (parent)");
        System.out.println("Name: " + ikan.name);
        ikan.golongan();

        //output dari child nya
        System.out.println("\n \nObject cupang (anak)");
        System.out.println("Name: " + cupang.name);
        System.out.println("Color: " + cupang.warna);
        cupang.golongan();
    }
}

class Ikan{
    String name;

    void golongan()
    {
        System.out.println("Ikan Petarung");
    }
}
class Cupang extends Ikan{
    String warna;

    void golongan() {
        System.out.println("Ikan air tawar dan petarung");
    }

}
