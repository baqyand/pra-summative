
public class Day5Task121 extends Day5Task122{
    public Day5Task121(){
        System.out.println("Sepeda gunung");
    }
    public void kecepatan(){
        System.out.println("kecepatan tergantung gigi yang di pakai");
    }
    public static void main(String[] args){
	// menjadikan day5 sebagai object
        Day5Task121 day5Task121 = new Day5Task121();
	// memanggil semua method
        day5Task121.tipeSepeda();
        day5Task121.merek();
        day5Task121.kecepatan();
    }
}
