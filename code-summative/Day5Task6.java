
public class Day5Task6 {
    public static void main(String[] args) {
        //inisialisasi local variable
        int loop = 10;

        for (int i = 1; i <= loop; i++) {
            //expression jika i == 6 maka akan di lewat
            if (i == 6){
                continue;
            }
            //output yang di keluarkan dan index 6 menghilang
            System.out.println("ini index : " + i);
        }
    }
}
