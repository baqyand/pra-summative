import java.util.ArrayList;

public class Day5Task8 {
    public static void main(String[] args){
	//inisialisasi arraylist motor
        ArrayList<String> motor = new ArrayList<String>();
	// mengisi arraylist dengan method.add
        motor.add("Beat");
        motor.add("Vario");
        motor.add("Aerox");
        motor.add("Nmax");
	
        System.out.println("sebelum di looping " );
        System.out.println(motor);
        //setelah di looping
        System.out.println("\n Setelah di looping ");
        for (String data : motor){
            System.out.println(data);
        }
    }
}
