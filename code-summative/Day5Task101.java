
public class Day5Task101 extends Day5Task102{

    public static void main(String[] args){
        // menginisialisasikan object day5
        Day5Task101 day5Task101 = new Day5Task101();

        //ini merupakan method yang terdapat di clas Day5Task102
        day5Task101.berlari();

        //output dari methodnya;
        day5Task101.kehidupan();
    }

    public void berlari() {
        int umur = 5;
        System.out.println("Makhluk hidup dapat berlari pada umur : " + umur);
    }
}
