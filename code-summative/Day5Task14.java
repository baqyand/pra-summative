
public class Day5Task14 extends Manusia {
    // melakukan override di sini
    public void eat() {
        System.out.println("Bama sedang puasa Ternyata");
    }
    public void run() {
        System.out.println("Ternyata bama males berlari");
    }

    // jika memakai super maka hasilnya akan sama dengan class manusia
    public static void main(String[] args) {
        Day5Task14 day = new Day5Task14();
        day.eat();
        day.run();
    }
}

class Manusia {
    //Overridden method
    public void eat() {
        System.out.println("Bama Sedang makan Guys ");
    }

    public void run() {
        System.out.println("Bama sedang berlari");
    }

}
