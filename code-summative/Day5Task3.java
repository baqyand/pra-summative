import java.util.Scanner;

public class Day5Task3 {
    public static void main(String[] args){
        //program switch statement
        //inisialisasi variable
        //inisialisasi inpud
        Scanner inp = new Scanner(System.in);
        int input ;

        System.out.println("====== Selamat Datang ======");
        System.out.println("1. Nama Saya  ");
        System.out.println("2. Umur  ");
        System.out.println("3. Email ");
        System.out.println("4. Tempat tinggal");
        System.out.println("5. Exit");
        System.out.print("Silakan pilih [1/2/3/4/5] : ");
        input = inp.nextInt();
	//switch untuk menentukan output
        switch (input){
            case 1:
                System.out.println("Bama Qyandija Deandra ");
                break;
            case 2:
                System.out.println("18 Tahun");
                break;
            case 3:
                System.out.println("bamaqyan@gmail.com");
                break;
            case 4:
                System.out.println("Cicalengka");
                break;
            case 5:
                System.out.println("Terimakasih telah memilih ");
                break;
            default:
                System.out.println("Terimakasih :) ");

        }

    }
}
