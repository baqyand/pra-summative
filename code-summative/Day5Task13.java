import java.util.Scanner;

public class Day5Task13 {
    public static void main(String[] args) {
        Scanner in  = new Scanner(System.in);
	//inisialisasi tipe data untuk inpud
        int a,b,input ;
        System.out.print("silakan memasukan angka 1 : ");
        a = in.nextInt();
        System.out.print("silakan memasukan angka 2 : ");
        b = in.nextInt();

        System.out.println("====== Selamat Datang ======");
        System.out.println("1. Tambah ");
        System.out.println("2. Kurang ");
        System.out.println("3. kali");
        System.out.println("4. Bagi ");
        System.out.println("5. Exit");
        System.out.print("Silakan pilih [1/2/3/4/5] : ");

        input = in.nextInt();
	//menambahkan switch statement untuk operasi matematika
        switch (input){
            case 1:
                System.out.print("Hasil adalah : ");
                tambah(a,b);
                break;
            case 2:
                System.out.print("Hasil adalah : ");
                kurang(a,b);
                break;
            case 3:
                System.out.print("Hasil adalah : ");
                kali(a,b);
                break;
            case 4:
                System.out.print("Hasil adalah : ");
                bagi(a,b);
                break;
            case 5:
                System.out.println("Terimakasih telah memilih ");
                break;
            default:
                System.out.println("Terimakasih :) ");

        }


    }

    public static int kali(int a, int b) {
        System.out.println(a * b);
        return a * b;
    }

    public static int tambah(int a, int b) {
        System.out.println(a + b);
        return a + b;
    }

    public static int kurang(int a, int b) {
        System.out.println(a - b);
        return a - b;
    }

    public static double bagi(int a, int b) {
        System.out.println(a / b);
        return a / b;
    }
}
