
public class Day5Task11 {
 public static void main(String[] args){
        Student s1 = new Student();
	// inisialisasi object sebelum namanya di set, sehingga hasil dari jvmnya null
        System.out.println("sebelum di set");
        System.out.println(s1.getName());
	//set nama
        s1.setName("Bama Qyandija");
	//output sesudah di set nama
        System.out.println("\n\nsesudah di set");
        System.out.println(s1.getName());

    }
}
class Student{
    private String name;
    public String getName() { return name;

    }
    public void setName(String name) { this.name = name; }

}
