
import java.util.Scanner;

public class Day5Task2 {
    public static void main(String[] args){
	//inisiasi inpud agar bisa di inpud
        Scanner inp = new Scanner(System.in);

        System.out.print("silakan memasukan umur : ");
        int age = inp.nextInt();

	//di sini terdapat ternary operator di mana jika umur lebih besar dari 16 maka akan muncul umur
        String result = (age >= 16) ? "umurnya lebih besar dari 16." : "umurnya lebih kecil 16.";
        System.out.println("\n" + result);
    }
}
