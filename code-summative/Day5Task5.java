
public class Day5Task5 {
    public static void main(String[] args) {
        //inisialisasi local variable
        int loop = 10;
	
	//di sini terdapat for loop di mana for loop tersebut akan break ketika index = 6
        for (int i = 1; i <= loop; i++) {
            System.out.println("ini indeks ke : " + i);
            if (i == 6){
                break;
            }
        }
    }
}
